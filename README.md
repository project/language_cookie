# Language Cookie

Language Cookie module adds an extra "Cookie" field to the Language Negotiation settings, allowing
the language to be set according to a cookie.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/language_cookie).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/language_cookie).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Enable the module and go to: Administration » Configuration » Regional and
  language » Languages » Detection and selection (available at the following
  URL: **admin/config/regional/language/detection/language_cookie**).

- Enable the "Cookie" detection method and re-arrange the user interface
  language detection as you see fit. The language cookie will be set to
  whichever language the user interface turns out to be in. The recommended
  setup is (from top to bottom):
    **URL -> Cookie -> Language Selection Page -> Default**.

  Just to illustrate, another possible setup is:
    **User -> Session -> Cookie -> Default**

- If you use URL-based caching such as Varnish and want the cookie to be set on
  all pages, there is a setting for this in the administration interface: Go to
  **admin/config/regional/language/configure/cookie** and check the relevant
  checkbox. If you want cookie-based automatic language selection without
  invoking Drupal, you will need to implement a Javascript based solution which
  reads the cookie and redirects the user to the right page.


  ## Maintainers

- Alex Weber - [alexweber](https://www.drupal.org/u/alexweber)
- Jeroen Tubex - [jeroent](https://www.drupal.org/u/jeroent)
- Stefan Ruijsenaars - [stefan.r](https://www.drupal.org/u/stefanr-0)
