<?php

namespace Drupal\language_cookie_test\Plugin\LanguageNegotiation;

use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language from a selected language.
 *
 * @LanguageNegotiation(
 *   id = \Drupal\language_cookie_test\Plugin\LanguageNegotiation\LanguageCookieFixedNegotiation::METHOD_ID,
 *   weight = 10,
 *   name = @Translation("Test language cookie negotiation"),
 *   description = @Translation("This is a test negotiation for language cookie."),
 *   types = {Drupal\Core\Language\LanguageInterface::TYPE_INTERFACE}
 * )
 */
class LanguageCookieFixedNegotiation extends LanguageNegotiationMethodBase {

  /**
   * The language negotiation method id.
   */
  const METHOD_ID = 'test-language-cookie-fixed-negotiation';

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    return 'nl';
  }

}
