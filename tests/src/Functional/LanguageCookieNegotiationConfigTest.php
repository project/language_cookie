<?php

namespace Drupal\Tests\language_cookie\Functional;

/**
 * Test the language cookie negotiation config.
 *
 * @group language_cookie
 */
class LanguageCookieNegotiationConfigTest extends LanguageCookieTestBase {

  /**
   * Test the language_cookie default config.
   */
  public function testLanguageCookieDefaultConfig() {
    $config = $this->config('language_cookie.negotiation');
    $this->assertEquals('language', $config->get('param'));
    $this->assertEquals(31536000, $config->get('time'));
    $this->assertEquals('/', $config->get('path'));
    $this->assertEquals('', $config->get('domain'));
    $this->assertFalse($config->get('secure'));
    $this->assertTrue($config->get('http_only'));
    $this->assertEquals('language_interface', $config->get('language_type'));
    $this->assertFalse($config->get('set_on_every_pageload'));
    $this->assertEquals([], $config->get('blacklisted_paths'));
  }

  /**
   * Test access to the language_cookie negotiation form.
   */
  public function testLanguageCookieNegotiationConfigFormAccess() {
    // Test access for admin user.
    $this->drupalGet('admin/config/regional/language/detection/language_cookie');
    $this->assertSession()->statusCodeEquals(200);

    // Test access for user with 'administer languages' permission.
    $user = $this->drupalCreateUser(['administer languages']);
    $this->drupalLogin($user);
    $this->drupalGet('admin/config/regional/language/detection/language_cookie');
    $this->assertSession()->statusCodeEquals(200);

    // Test access for user without 'administer languages' permission.
    $user = $this->drupalCreateUser();
    $this->drupalLogin($user);
    $this->drupalGet('admin/config/regional/language/detection/language_cookie');
    $this->assertSession()->statusCodeEquals(403);

    // Test access for anonymous user.
    $this->drupalLogout();
    $this->drupalGet('admin/config/regional/language/detection/language_cookie');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test the language_cookie negotiation config form.
   */
  public function testLanguageCookieNegotiationConfigForm() {
    $this->drupalGet('admin/config/regional/language/detection/language_cookie');
    $this->submitForm([
      'param' => 'language_cookie',
      'time' => 100000,
      'path' => '/drupal',
      'domain' => 'example.com',
      'secure' => TRUE,
      'http_only' => TRUE,
      'set_on_every_pageload' => TRUE,
      'blacklisted_paths' => '/admin/*' . PHP_EOL . '/user/*',
    ], 'Save configuration');

    $config = $this->config('language_cookie.negotiation');
    $this->assertEquals('language_cookie', $config->get('param'));
    $this->assertEquals(100000, $config->get('time'));
    $this->assertEquals('/drupal', $config->get('path'));
    $this->assertEquals('example.com', $config->get('domain'));
    $this->assertTrue($config->get('secure'));
    $this->assertTrue($config->get('http_only'));
    $this->assertTrue($config->get('set_on_every_pageload'));
    $this->assertEquals([
      '/admin/*',
      '/user/*',
    ], $config->get('blacklisted_paths'));

    $this->drupalGet('admin/config/regional/language/detection/language_cookie');
    $this->assertSession()->fieldValueEquals('param', 'language_cookie');
    $this->assertSession()->fieldValueEquals('time', 100000);
    $this->assertSession()->fieldValueEquals('path', '/drupal');
    $this->assertSession()->fieldValueEquals('domain', 'example.com');
    $this->assertSession()->checkboxChecked('secure');
    $this->assertSession()->checkboxChecked('http_only');
    $this->assertSession()->checkboxChecked('set_on_every_pageload');
    $this->assertSession()->fieldValueEquals('blacklisted_paths', '/admin/*' . PHP_EOL . '/user/*');
  }

  /**
   * Test the language_cookie param config option.
   */
  public function testLanguageCookieParamOption() {
    $this->getSession()->setCookie('language');
    $config = $this->config('language_cookie.negotiation');
    $config->set('param', 'language_cookie');
    $config->save();

    $node = $this->drupalCreateNode();
    $this->drupalGet('fr/node/' . $node->id());

    $this->assertEmpty($this->getSession()->getCookie('language'));
    $this->assertSession()->cookieEquals('language_cookie', 'fr');
  }

  /**
   * Test the language_cookie time config option.
   */
  public function testLanguageCookieTimeOption() {
    $config = $this->config('language_cookie.negotiation');
    $config->set('time', 60 * 60 * 24);
    $config->save();

    $node = $this->drupalCreateNode();
    $this->drupalGet('fr/node/' . $node->id());

    $this->assertMatchesRegularExpression('/max-age=86/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie contains expires option.');

    $config->set('time', 60 * 60 * 24 * 2);
    $config->save();

    $node = $this->drupalCreateNode();
    $this->drupalGet('en/node/' . $node->id());

    $this->assertMatchesRegularExpression('/max-age=172/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie contains expires option.');
  }

  /**
   * Test the language_cookie param config option.
   */
  public function testLanguageCookiePathOption() {
    $config = $this->config('language_cookie.negotiation');
    $config->set('path', '/drupal');
    $config->save();

    $node = $this->drupalCreateNode();
    $this->drupalGet('fr/node/' . $node->id());
    $this->assertMatchesRegularExpression('/path=\/drupal;/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie contains path option.');
  }

  /**
   * Test the language_cookie domain config option.
   */
  public function testLanguageCookieDomainOption() {
    $config = $this->config('language_cookie.negotiation');
    $config->set('domain', 'example.org');
    $config->save();

    $node = $this->drupalCreateNode();
    $this->drupalGet('fr/node/' . $node->id());

    $this->assertMatchesRegularExpression('/domain=example\.org;/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie contains domain option.');
  }

  /**
   * Test the language_cookie secure config option.
   */
  public function testLanguageCookieSecureOption() {
    $config = $this->config('language_cookie.negotiation');
    $config->set('secure', TRUE);
    $config->save();

    $node = $this->drupalCreateNode();
    $this->drupalGet('fr/node/' . $node->id());

    $this->assertMatchesRegularExpression('/secure/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie contains secure option.');
  }

  /**
   * Test the language_cookie http_only config option.
   */
  public function testLanguageCookieHttpOnlyOption() {
    $config = $this->config('language_cookie.negotiation');
    $config->set('http_only', TRUE);
    $config->save();

    $node = $this->drupalCreateNode();
    $this->drupalGet('fr/node/' . $node->id());

    $this->assertMatchesRegularExpression('/httponly/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie contains httpOnly option.');

    $config->set('http_only', FALSE);
    $config->save();

    $this->drupalGet('en/node/' . $node->id());
    $this->assertDoesNotMatchRegularExpression('/httponly/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie contains httpOnly option.');
  }

  /**
   * Test the language_cookie set_on_every_pageload config option.
   */
  public function testLanguageCookieSetOnEveryPageLoadOption() {
    $node = $this->drupalCreateNode();
    $this->drupalGet('fr/node/' . $node->id());
    $this->assertMatchesRegularExpression('/language=fr/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie is set.');

    $this->drupalGet('fr/node/' . $node->id());
    $this->assertEmpty($this->getSession()->getResponseHeader('Set-Cookie'));

    $this->drupalGet('en/node/' . $node->id());
    $this->assertMatchesRegularExpression('/language=en/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie is set.');

    $config = $this->config('language_cookie.negotiation');
    $config->set('set_on_every_pageload', TRUE);
    $config->save();

    $node = $this->drupalCreateNode();
    $this->drupalGet('fr/node/' . $node->id());
    $this->assertMatchesRegularExpression('/language=fr/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie is set.');

    $this->drupalGet('fr/node/' . $node->id());
    $this->assertMatchesRegularExpression('/language=fr/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie is set.');

    $this->drupalGet('en/node/' . $node->id());
    $this->assertMatchesRegularExpression('/language=en/i', $this->getSession()->getResponseHeader('Set-Cookie'), 'Cookie is set.');
  }

}
