<?php

namespace Drupal\Tests\language_cookie\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\language_cookie\Plugin\LanguageNegotiation\LanguageNegotiationCookie;
use Drupal\language_cookie_test\Plugin\LanguageNegotiation\LanguageCookieFixedNegotiation;
use Drupal\Tests\BrowserTestBase;

/**
 * Base Class for Language Cookie Tests.
 */
abstract class LanguageCookieTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language_cookie',
    'content_translation',
    'language_cookie_test',
    'node',
    'path',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);
    $this->drupalCreateContentType(['type' => 'page']);
    $this->config('system.site')->set('page.front', '/node')->save();
    // Create FR.
    ConfigurableLanguage::createFromLangcode('fr')->save();
    // Set prefixes to en and fr.
    $this->drupalGet('admin/config/regional/language/detection/url');
    $this->submitForm([
      'prefix[en]' => 'en',
      'prefix[fr]' => 'fr',
    ], 'Save configuration');
    // Set up URL, our fixed test negotiation and language cookie methods.
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][' . LanguageNegotiationCookie::METHOD_ID . ']' => 1,
      'language_interface[enabled][language-url]' => 1,
      'language_interface[enabled][' . LanguageCookieFixedNegotiation::METHOD_ID . ']' => 1,
    ], 'Save settings');
    // Turn on content translation for pages.
    $config = ContentLanguageSettings::loadByEntityTypeBundle('node', 'page');
    $config->setDefaultLangcode('en')
      ->setLanguageAlterable(TRUE)
      ->save();
  }

}
