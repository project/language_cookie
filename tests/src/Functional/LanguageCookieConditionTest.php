<?php

namespace Drupal\Tests\language_cookie\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests that the condition plugins work.
 *
 * @group language_cookie
 */
class LanguageCookieConditionTest extends LanguageCookieTestBase {

  /**
   * Test the "Blacklisted paths" condition.
   */
  public function testBlackListedPaths() {
    $node = $this->drupalCreateNode();

    // Remove cookie.
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('node/' . $node->id());
    $last = $this->container->get('state')->get('language_cookie_test.language_negotiation_last');
    $last_interface_language = $last[LanguageInterface::TYPE_INTERFACE];
    $this->assertEquals('en', $last_interface_language);

    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('fr/node/' . $node->id());
    $this->assertSession()->cookieEquals('language', 'fr');

    // Test that the newly set cookie sets the language to french.
    $this->drupalGet('node/' . $node->id());
    $last = $this->container->get('state')->get('language_cookie_test.language_negotiation_last');
    $last_interface_language = $last[LanguageInterface::TYPE_INTERFACE];
    $this->assertEquals('fr', $last_interface_language);

    // Add node to blacklisted paths.
    $this->drupalGet('en/admin/config/regional/language/detection/language_cookie');
    $this->submitForm(['blacklisted_paths' => '/admin/*' . PHP_EOL . '/node/' . $node->id()], 'Save configuration');
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('en/node/' . $node->id());
    $this->assertEmpty($this->getSession()->getCookie('language'));

    // Add node to blacklisted paths (in the middle).
    $this->drupalGet('en/admin/config/regional/language/detection/language_cookie');
    $this->submitForm(['blacklisted_paths' => '/admin/*' . PHP_EOL . '/node/' . $node->id() . PHP_EOL . '/bar'], 'Save configuration');
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('en/node/' . $node->id());
    $this->assertEmpty($this->getSession()->getCookie('language'));

    // Add string that contains node, but not node itself.
    $this->drupalGet('en/admin/config/regional/language/detection/language_cookie');
    $this->submitForm(['blacklisted_paths' => '/admin/*' . PHP_EOL . '/node/' . $node->id() . '/foobar' . PHP_EOL . '/bar'], 'Save configuration');
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('en/node/' . $node->id());
    $this->assertSession()->cookieEquals('language', 'en');

    // Add string that starts with node, but not node itself.
    $this->drupalGet('en/admin/config/regional/language/detection/language_cookie');
    $this->submitForm(['blacklisted_paths' => '/admin/*' . PHP_EOL . '/node/' . $node->id() . '/foobar'], 'Save configuration');
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('en/node/' . $node->id());
    $this->assertSession()->cookieEquals('language', 'en');

    // Test front page.
    $this->drupalGet('en/admin/config/regional/language/detection/language_cookie');
    $this->submitForm(['blacklisted_paths' => '/admin/*'], 'Save configuration');
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('en');
    $this->assertSession()->cookieEquals('language', 'en');

    $this->drupalGet('en/admin/config/regional/language/detection/language_cookie');
    $this->submitForm(['blacklisted_paths' => '/admin/*' . PHP_EOL . '<front>'], 'Save configuration');
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('en');
    $this->assertEmpty($this->getSession()->getCookie('language'));

    // Test hardcoded blacklist.
    $this->drupalGet('admin/config/search/path/add');
    $this->submitForm([
      'langcode[0][value]' => 'und',
      'path[0][value]' => '/node/' . $node->id(),
      // We create this alias just to test that the hardcoded paths ported
      // from the Drupal 7 version of this module are also picked up.
      'alias[0][value]' => '/httprl_async_function_callback',
    ], 'Save');
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('fr/httprl_async_function_callback');

    // Check that cookie is not set.
    $this->assertEmpty($this->getSession()->getCookie('language'));
  }

  /**
   * Test the "xml_http_request" condition.
   */
  public function testAjax() {
    $node = $this->drupalCreateNode();
    $headers = [];
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('fr/node/' . $node->id(), [], $headers);
    $this->assertSession()->cookieEquals('language', 'fr');
    $headers['X-Requested-With'] = 'XMLHttpRequest';
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('fr/node/' . $node->id(), [], $headers);
    $this->assertEmpty($this->getSession()->getCookie('language'));
  }

  /**
   * Test that language_cookie also works with only 1 language.
   */
  public function testOneLanguage() {
    // Remove the second language.
    $language = ConfigurableLanguage::load('fr');
    $language->delete();

    $node = $this->drupalCreateNode();

    // Check that the cookie is correctly set.
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('en/node/' . $node->id());
    $this->assertEmpty($this->getSession()->getCookie('language'));

    // Make sure that clearing the caches works with 1 language.
    $this->drupalGet('admin/config/development/performance');
    $this->submitForm([], 'Clear all caches');
  }

}
