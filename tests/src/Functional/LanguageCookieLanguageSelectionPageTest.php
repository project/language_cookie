<?php

namespace Drupal\Tests\language_cookie\Functional;

use Drupal\language_cookie\Plugin\LanguageNegotiation\LanguageNegotiationCookie;
use Drupal\language_cookie_test\Plugin\LanguageNegotiation\LanguageCookieFixedNegotiation;

/**
 * Test that the language_cookie module works well with language_selection_page.
 *
 * @group language_cookie
 */
class LanguageCookieLanguageSelectionPageTest extends LanguageCookieTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language_selection_page',
  ];

  /**
   * Text to assert for to determine if we are on the Language Selection Page.
   */
  protected const LANGUAGE_SELECTION_PAGE_TEXT = 'This page is the default page of the module Language Selection Page';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][' . LanguageNegotiationCookie::METHOD_ID . ']' => FALSE,
      'language_interface[enabled][' . LanguageCookieFixedNegotiation::METHOD_ID . ']' => FALSE,
      'language_interface[enabled][language-selection-page]' => 1,
      'language_interface[enabled][language-url]' => 1,
    ], 'Save settings');
  }

  /**
   * Test that the language cookie and the language selection page work.
   */
  public function testLanguageCookieAndSelectionPage() {
    // Test that no cookie is set when the module is enabled but not configured.
    $node = $this->drupalCreateNode();
    $this->drupalGet('node/' . $node->id());
    $this->assertLanguageSelectionPageLoaded();

    // Enable cookie.
    $this->drupalGet('admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][language-url]' => 1,
      'language_interface[enabled][' . LanguageNegotiationCookie::METHOD_ID . ']' => 1,
      'language_interface[enabled][language-selection-page]' => 1,
      'language_interface[weight][language-url]' => -8,
      'language_interface[weight][' . LanguageNegotiationCookie::METHOD_ID . ']' => -5,
      'language_interface[weight][language-selection-page]' => -4,
    ], 'Save settings');
    $this->assertSession()->cookieEquals('language', 'en');
    // Remove cookie.
    $this->getSession()->setCookie('language', NULL);
    $this->drupalGet('node/' . $node->id());
    $this->assertLanguageSelectionPageLoaded();
    $this->assertSession()->responseContains('en/node/' . $node->id());
    // Cookie should not yet be set.
    $this->assertEmpty($this->getSession()->getCookie('language'));
    $this->clickLink('English');
    // Cookie should be set at this point.
    $this->assertSession()->cookieEquals('language', 'en');
  }

  /**
   * Assert that the language selection page is loaded.
   */
  protected function assertLanguageSelectionPageLoaded() {
    $this->assertSession()->pageTextContains(self::LANGUAGE_SELECTION_PAGE_TEXT);
  }

  /**
   * Assert that the language selection page is not loaded.
   */
  protected function assertLanguageSelectionPageNotLoaded() {
    $this->assertSession()->pageTextNotContains(self::LANGUAGE_SELECTION_PAGE_TEXT);
  }

}
