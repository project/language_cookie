<?php

namespace Drupal\Tests\language_cookie\Functional;

/**
 * Tests that this module works in combination with language_access..
 *
 * @group language_cookie
 */
class LanguageCookieLanguageAccessTest extends LanguageCookieTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['language_access'];

  /**
   * Test the module in combination with language_access.
   */
  public function testLanguageAccess() {
    $node = $this->drupalCreateNode();

    $user = $this->createUser(['access language en']);
    $this->drupalLogin($user);

    // Remove cookie.
    $this->getSession()->setCookie('language');

    // The user doesn't have access to French, so no cookie should be set.
    $this->drupalGet('fr/node/' . $node->id());
    $this->assertEmpty($this->getSession()->getCookie('language'));

    // The user does have access to English, so the cookie should be set.
    $this->drupalGet('en/node/' . $node->id());
    $this->assertSession()->cookieEquals('language', 'en');

    // Create a user with access to French and English.
    $user = $this->createUser(['access language fr', 'access language en']);
    $this->drupalLogin($user);

    // Remove cookie.
    $this->getSession()->setCookie('language');

    // This user does have access to French, so the cookie should be set.
    $this->drupalGet('fr/node/' . $node->id());
    $this->assertSession()->cookieEquals('language', 'fr');
  }

}
