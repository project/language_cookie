<?php

namespace Drupal\Tests\language_cookie\Functional;

/**
 * Tests that the condition plugins work.
 *
 * @group language_cookie
 */
class LanguageCookiePageCacheTest extends LanguageCookieTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'page_cache',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $config = $this->config('system.performance');
    $config->set('cache.page.max_age', 300);
    $config->save();
  }

  /**
   * Test that the language cookie module works with page_cache enabled.
   */
  public function testLanguageCookiePageCache() {
    // Create a node and add a translation.
    $node = $this->drupalCreateNode(['title' => 'Title EN']);
    $node->addTranslation('fr', ['title' => 'Title FR']);
    $node->save();

    $this->drupalLogout();

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->titleEquals('Title EN | Drupal');

    $this->drupalGet('/fr/user');

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->titleEquals('Title FR | Drupal');
  }

  /**
   * Test that the language cookie module works with page_cache enabled.
   */
  public function testLanguageCookieFrontPageCache() {
    // Create a node and add a translation.
    $node = $this->drupalCreateNode(['title' => 'Title EN']);
    $node->addTranslation('fr', ['title' => 'Title FR']);
    $node->save();

    $this->config('system.site')
      ->set('page.front', '/node/' . $node->id())
      ->save();
    $this->drupalLogout();

    $this->drupalGet('/fr/user');

    $this->drupalGet('');
    $this->assertSession()->titleEquals('Title FR | Drupal');
  }

}
