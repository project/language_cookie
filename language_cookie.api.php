<?php

/**
 * @file
 * Documentation for Language Cookie module APIs.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Modify the list of available Language Cookie condition plugins.
 *
 * This hook may be used to modify plugin properties after they have been
 * specified by other modules.
 *
 * @param array &$plugins
 *   An array of all the existing plugin definitions, passed by reference.
 *
 * @see \Drupal\language_cookie\LanguageCookieConditionManager
 */
function hook_language_cookie_condition_info_alter(array &$plugins) {
  $plugins['some_plugin']['name'] = t('Better name');
}

/**
 * Modify the language cookie that is set by this module.
 *
 * @param \Symfony\Component\HttpFoundation\Cookie &$cookie
 *   The actual cookie, passed by reference.
 *
 * @see \Drupal\language_cookie\EventSubscriber\LanguageCookieSubscriber::setLanguageCookie()
 */
function hook_language_cookie_alter(Cookie &$cookie) {
  $default_langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $cookie = \Symfony\Component\HttpFoundation\Cookie::create('my_altered_language_cookie', $default_langcode);
}

/**
 * @} End of "addtogroup hooks".
 */
