<?php

namespace Drupal\language_cookie;

use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * The language cookie condition interface.
 */
interface LanguageCookieConditionInterface extends ConditionInterface {

  /**
   * Wrapper function that returns FALSE.
   *
   * @return bool
   *   Return FALSE
   */
  public function block();

  /**
   * Wrapper function that returns FALSE.
   *
   * @return bool
   *   Return TRUE
   */
  public function pass();

  /**
   * Returns the name of the plugin.
   *
   * If the name is not set, returns its ID.
   *
   * @return string
   *   The name of the plugin.
   */
  public function getName();

  /**
   * Returns the description of the plugin.
   *
   * If the description is not set, returns NULL.
   *
   * @return string|null
   *   The description of the plugin.
   */
  public function getDescription();

  /**
   * Returns the weight of the plugin.
   *
   * If the weight is not set, returns 0.
   *
   * @return int
   *   The weight of the plugin.
   */
  public function getWeight();

  /**
   * Set the weight of the plugin.
   *
   * @param int $weight
   *   The plugin's weight.
   *
   * @return $this
   *   Returns itself.
   */
  public function setWeight($weight);

  /**
   * Post config save method.
   *
   * Method that gets triggered when the configuration of the form
   * has been saved.
   *
   * @param \Drupal\Core\Config\Config $config
   *   The config object.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   */
  public function postConfigSave(Config $config, array &$form, FormStateInterface $form_state);

  /**
   * Set the current language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The current language object.
   *
   * @return $this
   *   Returns itself.
   */
  public function setCurrentLanguage(LanguageInterface $language);

  /**
   * Get the current language.
   *
   * @return \Drupal\Core\Language\LanguageInterface|null
   *   The current language when available, else NULL.
   */
  public function getCurrentLanguage();

}
