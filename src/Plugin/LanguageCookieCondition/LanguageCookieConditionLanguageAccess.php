<?php

namespace Drupal\language_cookie\Plugin\LanguageCookieCondition;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\language_cookie\LanguageCookieConditionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for language access plugin.
 *
 * This plugin needs the language access. When this module is enabled, this
 * plugin will check if the current user has access to the current page.
 *
 * @LanguageCookieCondition(
 *   id = "language_access",
 *   weight = -120,
 *   name = @Translation("Language Access"),
 *   description = @Translation("Bails out when the Language Access module is enabled and the user doesn't have access to the current language."),
 * )
 */
class LanguageCookieConditionLanguageAccess extends LanguageCookieConditionBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a LanguageCookieConditionPath plugin.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(AccountInterface $current_user, ModuleHandlerInterface $module_handler, array $configuration, $plugin_id, array $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('current_user'),
      $container->get('module_handler'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $current_language = $this->getCurrentLanguage();
    if ($this->moduleHandler->moduleExists('language_access') && (!$current_language instanceof LanguageInterface || !$this->currentUser->hasPermission('access language ' . $current_language->getId()))) {
      return $this->block();
    }

    return $this->pass();
  }

}
