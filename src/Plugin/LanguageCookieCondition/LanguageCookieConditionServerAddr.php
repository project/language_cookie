<?php

namespace Drupal\language_cookie\Plugin\LanguageCookieCondition;

use Drupal\language_cookie\LanguageCookieConditionBase;

/**
 * Class for the Server Addr condition plugin.
 *
 * @LanguageCookieCondition(
 *   id = "server_addr",
 *   weight = -70,
 *   name = @Translation("Server Address condition check"),
 *   description = @Translation("Bails out if the server address is not set."),
 * )
 */
class LanguageCookieConditionServerAddr extends LanguageCookieConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (isset($_SERVER['SERVER_ADDR'])) {
      return $this->pass();
    }

    return $this->block();
  }

}
