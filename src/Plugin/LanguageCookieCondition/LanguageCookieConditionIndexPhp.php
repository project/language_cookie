<?php

namespace Drupal\language_cookie\Plugin\LanguageCookieCondition;

use Drupal\language_cookie\LanguageCookieConditionBase;

/**
 * Class for the Index.php condition plugin.
 *
 * @LanguageCookieCondition(
 *   id = "index",
 *   weight = -60,
 *   name = "Index.php",
 *   description = @Translation("Bails out when running the script on another php file than index.php."),
 * )
 */
class LanguageCookieConditionIndexPhp extends LanguageCookieConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if ($_SERVER['SCRIPT_NAME'] !== $GLOBALS['base_path'] . 'index.php') {
      return $this->block();
    }

    return $this->pass();
  }

}
