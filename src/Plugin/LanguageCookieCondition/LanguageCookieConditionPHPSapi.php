<?php

namespace Drupal\language_cookie\Plugin\LanguageCookieCondition;

use Drupal\language_cookie\LanguageCookieConditionBase;

/**
 * Class for the PHP Sapi condition plugin.
 *
 * @LanguageCookieCondition(
 *   id = "php_sapi",
 *   weight = -120,
 *   name = @Translation("PHP SAPI"),
 *   description = @Translation("Bails out when running on command line."),
 * )
 */
class LanguageCookieConditionPHPSapi extends LanguageCookieConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (PHP_SAPI === 'cli') {
      return $this->block();
    }

    return $this->pass();
  }

}
