<?php

namespace Drupal\language_cookie\Plugin\LanguageNegotiation;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language from a language cookie.
 *
 * The recommended order is URL > Cookie > Language Selection Page, so weight
 * is set to -5 by default so that it is lower than Language Selection Page
 * (see https://www.drupal.org/project/language_selection_page), which has a
 * weight of -4, and so that it higher than URL, which has a weight of -8.
 *
 * @LanguageNegotiation(
 *   weight = -5,
 *   name = @Translation("Cookie"),
 *   description = @Translation("Determine the language from a cookie"),
 *   id = Drupal\language_cookie\Plugin\LanguageNegotiation\LanguageNegotiationCookie::METHOD_ID,
 *   config_route_name = "language_cookie.negotiation_cookie"
 * )
 */
class LanguageNegotiationCookie extends LanguageNegotiationMethodBase implements ContainerFactoryPluginInterface {

  /**
   * The language negotiation method ID.
   */
  const METHOD_ID = 'language-cookie';

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The page cache kill switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $pageCacheKillSwitch;

  /**
   * Constructs a new LanguageNegotiationCookie instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $page_cache_kill_switch
   *   The page cache kill switch.
   */
  public function __construct(ConfigFactoryInterface $config_factory, KillSwitch $page_cache_kill_switch) {
    $this->configFactory = $config_factory;
    $this->pageCacheKillSwitch = $page_cache_kill_switch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('config.factory'),
      $container->get('page_cache_kill_switch')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    $langcode = NULL;
    $config = $this->configFactory->get('language_cookie.negotiation');
    $param = $config->get('param');

    if ($request->cookies->has($param) && in_array($request->cookies->get($param), array_keys($this->languageManager->getLanguages()))) {
      $langcode = $request->cookies->get($param);

      // Internal page cache with multiple languages and browser negotiation
      // could lead to wrong cached sites. Therefore disabling the internal page
      // cache.
      // @todo Solve more elegantly in https://www.drupal.org/node/2430335.
      $this->pageCacheKillSwitch->trigger();
    }

    return $langcode;
  }

}
