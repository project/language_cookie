<?php

namespace Drupal\language_cookie;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for language cookie condition.
 */
abstract class LanguageCookieConditionBase extends ConditionPluginBase implements LanguageCookieConditionInterface, ContainerFactoryPluginInterface {

  /**
   * The condition's weight, order of execution.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The current language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected $currentLanguage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function block() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function pass() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    // This should return a summary but it's not used in our case.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration[$this->getPluginId()] = $form_state->getValue($this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    $definition = $this->getPluginDefinition();
    return !empty($definition['name']) ? $definition['name'] : $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $definition = $this->getPluginDefinition();
    return !empty($definition['description']) ? $definition['description'] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return !empty($this->weight) ? $this->weight : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    return $this->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function postConfigSave(Config $config, array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function setCurrentLanguage(LanguageInterface $language) {
    $this->currentLanguage = $language;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentLanguage() {
    return $this->currentLanguage;
  }

}
