<?php

namespace Drupal\language_cookie\EventSubscriber;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Executable\ExecutableManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\language\LanguageNegotiatorInterface;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationSelected;
use Drupal\language_cookie\Plugin\LanguageNegotiation\LanguageNegotiationCookie;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Provides a LanguageCookieSubscriber.
 */
class LanguageCookieSubscriber implements EventSubscriberInterface {

  /**
   * The event.
   *
   * @var \Symfony\Component\HttpKernel\Event\ResponseEvent
   */
  protected $event;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The language negotiator.
   *
   * @var \Drupal\language\LanguageNegotiatorInterface
   */
  protected $languageNegotiator;

  /**
   * The Language Cookie condition plugin manager.
   *
   * @var \Drupal\Core\Executable\ExecutableManagerInterface
   */
  protected $languageCookieConditionManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new class object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\language\LanguageNegotiatorInterface $language_negotiator
   *   The language negotiator.
   * @param \Drupal\Core\Executable\ExecutableManagerInterface $plugin_manager
   *   The language cookie condition plugin manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, LanguageNegotiatorInterface $language_negotiator, ExecutableManagerInterface $plugin_manager, ModuleHandlerInterface $module_handler, TimeInterface $time) {
    $this->languageManager = $language_manager;
    $this->configFactory = $config_factory;
    $this->languageNegotiator = $language_negotiator;
    $this->languageCookieConditionManager = $plugin_manager;
    $this->moduleHandler = $module_handler;
    $this->time = $time;
  }

  /**
   * Helper method that gets the language code to set the cookie to.
   *
   * Loops through all available language negotiation methods with higher
   * priority than the Language Cookie method itself.
   *
   * @see \Drupal\language_cookie\LanguageCookieSubscriber::setLanguageCookie()
   *
   * @return \Drupal\Core\Language\LanguageInterface|null
   *   An string with the language code or FALSE.
   */
  protected function getLanguage() {
    if (!$this->languageManager->isMultilingual()) {
      return $this->languageManager->getDefaultLanguage();
    }

    $config = $this->configFactory->get('language_cookie.negotiation');
    // In the install hook for this module, we assume the interface language
    // will be used to set the cookie. If you want to use another language
    // negotiation type instead (ie. content/URL), you can use "language_type"
    // config key.
    $type = $config->get('language_type');
    // Get all methods available for this language type.
    $methods = $this->languageNegotiator->getNegotiationMethods($type);

    // Sort the language negotiation methods by weight.
    uasort($methods, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

    // We ignore this language method or else it will always return a language.
    unset($methods[LanguageNegotiationSelected::METHOD_ID]);

    foreach ($methods as $method_id => $method_definition) {
      // Do not consider language providers with a lower priority than the
      // cookie language provider, nor the cookie provider itself.
      if ($method_id === LanguageNegotiationCookie::METHOD_ID) {
        return NULL;
      }
      $language_id = $this->languageNegotiator->getNegotiationMethodInstance($method_id)->getLangcode($this->event->getRequest());
      if (!empty($language_id)) {
        return $this->languageManager->getLanguage($language_id);
      }
    }

    // If no other language was found, use the default one.
    return $this->languageManager->getDefaultLanguage();
  }

  /**
   * Event callback for setting the language cookie.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The response event.
   *
   * @return bool
   *   - FALSE if a condition plugin prevented the cookie from being set.
   *   - TRUE if all conditions pass. If a language is available, the cookie
   *     will have been set.
   */
  public function setLanguageCookie(ResponseEvent $event) {
    $this->event = $event;
    $config = $this->configFactory->get('language_cookie.negotiation');

    $manager = $this->languageCookieConditionManager;

    // Get the current language to set in the cookie to by running through all
    // language negotiation methods with higher priority (in terms of weight)
    // than the Language Cookie method.
    $language = $this->getLanguage();
    if (!$language instanceof LanguageInterface) {
      return FALSE;
    }

    // Run through the condition plugins that may prevent a cookie from being
    // set.
    foreach ($manager->getDefinitions() as $def) {
      /** @var \Drupal\language_cookie\LanguageCookieConditionInterface $condition_plugin */
      $condition_plugin = $manager->createInstance($def['id'], $config->get());
      $condition_plugin->setCurrentLanguage($language);
      if (!$manager->execute($condition_plugin)) {
        return FALSE;
      }
    }

    $request = $this->event->getRequest();

    // Get the name of the cookie parameter.
    $param = $config->get('param');

    if ((!$request->cookies->has($param) || ($request->cookies->get($param) != $language->getId())) || $config->get('set_on_every_pageload')) {
      $cookie = Cookie::create($param, $language->getId(), $this->time->getRequestTime() + $config->get('time'), $config->get('path'), $config->get('domain'), $config->get('secure'), $config->get('http_only'));
      // Allow other modules to change the $cookie.
      $this->moduleHandler->alter('language_cookie', $cookie);
      $this->event->getResponse()->headers->setCookie($cookie);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['setLanguageCookie', 20];
    return $events;
  }

}
